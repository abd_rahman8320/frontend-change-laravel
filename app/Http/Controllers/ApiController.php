<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;

class ApiController extends Controller
{
    public function top_5_user_activity()
    {
        $get = Api::top_5_user_activity();

        return $get;
    }

    public function user_activity()
    {
        $get = Api::user_activity();

        return $get;
    }

    public function mml_command(Request $request)
    {
        $get = Api::mml_command($request->input('command'));

        return $get;
    }

    public function dafinci()
    {
        $data = Api::dafinci();

        return $data;
    }

    public function remedy()
    {
        $data = Api::remedy();

        return $data;
    }

    public function btsonair()
    {
        $data = Api::btsonair();

        return $data;
    }

    public function nodin()
    {
        $data = Api::nodin();

        return $data;
    }

    public function chart_btsonair()
    {
        $data = Api::chart_btsonair();

        return $data;
    }

    public function chart_nodin()
    {
        $data = Api::chart_nodin();

        return $data;
    }

    public function chart_user()
    {
        $data = Api::chart_user();

        return $data;
    }

    public function chart_dea()
    {
        $data = Api::chart_dea();

        return $data;
    }

    public function act_cell(){
        $data = Api::act_cell();
        return $data;
    }

    public function dea_cell(){
        $data = Api::dea_cell();
        return $data;
    }
    public function add_license(){
        $data = Api::add_license();
        return $data;
    }
    public function act_license(){
        $data = Api::act_license();
        return $data;
    }

    public function dea_license(){
        $data = Api::dea_license();
        return $data;
    }

    public function add_bts(){
        $data = Api::add_bts();
        return $data;
    }

    public function cell_deactivated(){
        $data = Api::cell_deactivated();
        return $data;
    }


}
