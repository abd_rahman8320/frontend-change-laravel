<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="{{url('')}}/css/app.css">
        <link rel="stylesheet" href="{{url('')}}/css/style.css">
        <link rel="stylesheet" href="{{url('')}}/custom/css/adminlte.min.css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    </head>
    <body>
        <div class="row">
            <div class="col-lg-6" style="background-color:grey;">
                <p style="color:white;">TELKOMSEL CHANGE MANAGEMENT DASHBOARD</p>
            </div>
            <div class="col-lg-6" style="background-color:grey;">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6" style="background-color:gray;height:100px">
            <div class="row">
                <div class="col-lg-3" style="background-color:black;height:100px;margin-right: 5px">
                    <h3>36 Cell</h3>

                    <p>Activated Cell</p>
                </div>
                <div class="col-lg-3" style="background-color:white;height:100px;margin-right: 5px">
                    <h3>36 Cell</h3>

                    <p>Activated Cell</p>
                </div>

                <div class="col-lg-3" style="background-color:black;height:100px;margin-right: 5px">

                </div>

                <div class="col-lg-3" style="background-color:white;height:100px">

                </div>
            </div>
            </div>
            <div class="col-lg-6" style="background-color:red;height:100px">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-6" style="background-color:grey;">
                <p style="color:white">COUNT OF CELL DEACTIVATED</p>
            </div>
            <div class="col-lg-6" style="background-color:grey;">
                <p style="color:white">COUNT OF LICENSE ACTIVATED</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6" style="background-color:blue;height:100px">

            </div>
            <div class="col-lg-6" style="background-color:red;height:100px">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12" style="background-color:grey;">
                <p style="color:white">COUNT OF MML COMMAND USER BY TIME</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12" style="background-color:black;height:100px">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12" style="background-color:grey;">
                <p style="color:white">LIST MML COMMAND</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12" style="background-color:black;height:100px">

            </div>
        </div>
    </body>

    <script type="text/javascript" src="{{url('')}}/js/app.js"></script>
    <!-- AdminLTE App -->
    <script src="{{url('')}}/custom/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
</html>
